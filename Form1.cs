﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GwentTools_Project
{
    public partial class Form1 : Form
    {
        private string m_ProjectRootPath = string.Empty;
        
        // Debug short cut project root
        //private string m_ProjectRootPath = "D:\\VS\\Gwent_Project";

        private string m_CurrentDeckPath = string.Empty;
        private List<JObject> m_CardList;

        public Form1()
        {
            InitializeComponent();
            cardListBox.Height = Height - 70;

            // Disable other buttons till project root set
            newToolStripMenuItem.Enabled = false;
            saveToolStripMenuItem.Enabled = false;
            saveAsNewToolStripMenuItem.Enabled = false;
            loadDeckToolStripMenuItem.Enabled = false;
        }


        // Load Deck button clicked
        private void loadDeckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Select deck files to load
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @m_ProjectRootPath,
                Title = "Select Deck file to load:",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "json",
                Filter = "json files (*.json)|*.json",
                FilterIndex = 2,
                RestoreDirectory = true,

            };

            if (openFileDialog.ShowDialog() == DialogResult.OK )
            {
                // Keep a reference for further use
                m_CurrentDeckPath = openFileDialog.FileName;
                m_CardList = DataManager.ReadJsonFile(openFileDialog.FileName);

                // Load cards into list box
                ReloadCardList();
            }

        }
        private void ResetAbilityList()
        {
            abilityValue1.SelectedIndex = 0;
            abilityValue2.SelectedIndex = 0;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Toggle between list box to view and edit
        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Toggle the info based on each item selected
            ResetAbilityList();
            if (cardListBox.SelectedIndex < 0 || cardListBox.SelectedIndex >= cardListBox.Items.Count)
            {
                return;
            }

            JObject selectedCard = m_CardList[cardListBox.SelectedIndex];

            // Display the selected card info on the panel
            nameValue.Text = selectedCard["name"].ToString();
            rowValue.SelectedIndex = rowValue.FindStringExact(selectedCard["row"].ToString());
            pointValue.Value = Convert.ToInt32( selectedCard["originalPoint"]);
            rarityValue.SelectedIndex = rarityValue.FindStringExact(selectedCard["rarity"].ToString());
            descValue.Text = selectedCard["description"].ToString();

            // Display Image
            if (selectedCard["image"].ToString() != "")
            {
                cardImage.ImageLocation = @m_ProjectRootPath + "\\CardImages\\" + selectedCard["image"].ToString();
            }
            else
            {
                cardImage.ImageLocation = null;
            }

            // Display Ability list
            JArray abilityList = JArray.FromObject(selectedCard["ability"]);

            for(int i = 0; i < abilityList.Count; i++)
            {
                ComboBox abilityValue = Controls.Find("abilityValue" + (i + 1), false)[0] as ComboBox;
                abilityValue.SelectedIndex = abilityValue.FindStringExact(abilityList[i].ToString());
            }


        }

        // Set project root path
        private void setProjectPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set the root project folder to find the target easily
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                string untestedPath = folderDialog.SelectedPath;

                if (Directory.Exists(untestedPath + "\\CardImages"))
                {
                    m_ProjectRootPath = untestedPath;

                    // activate other buttons
                    notificationText.Text = "";
                    newToolStripMenuItem.Enabled = true;
                    saveToolStripMenuItem.Enabled = true;
                    saveAsNewToolStripMenuItem.Enabled = true;
                    loadDeckToolStripMenuItem.Enabled = true;

                }
                else
                {
                    MessageBox.Show("Invalid Project root path");
                    return;
                }

            }
        }

        // Save to current deck clicked
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Save the data modified to the current json
            if (cardListBox.SelectedIndex != -1 )
            {
                JObject updatedObject = new JObject();

                updatedObject.Add("name", nameValue.Text);
                updatedObject.Add("row", rowValue.Text);
                updatedObject.Add("originalPoint", pointValue.Value);
                updatedObject.Add("description", descValue.Text);
                updatedObject.Add("rarity", rarityValue.Text);

                updatedObject.Add("image", Path.GetFileName(cardImage.ImageLocation));

                // add ability list
                JArray abilityList = GetAbilityList();
                updatedObject.Add("ability", abilityList);

                m_CardList[cardListBox.SelectedIndex] = updatedObject;

                DataManager.SaveJsonFile(m_CurrentDeckPath, JArray.FromObject(m_CardList));
            }
            else
            {
                MessageBox.Show("No card selected, maybe you show use Save as New or Load a deck");
                return;
            }
 
        }

        // Ensure the ability sequence right when changing value
        private void abilityValue1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set the ability 2 to empty if ability 1 is empty
            if (abilityValue1.SelectedIndex == 0)
            {
                abilityValue2.SelectedIndex = 0;
            }
        }

        private JArray GetAbilityList()
        {
            JArray abilityList = new JArray();
            // if ability 2 has value then 2 ability
            if (abilityValue2.SelectedIndex != 0)
            {
                abilityList.Add(abilityValue1.SelectedItem.ToString());
                abilityList.Add(abilityValue2.SelectedItem.ToString());

            }else if (abilityValue1.SelectedIndex != 0)
            {
                abilityList.Add(abilityValue1.SelectedItem.ToString());
            }

            return abilityList;

        }

        // change image clicked
        private void changeImageButton_Click(object sender, EventArgs e)
        {
            // check if that file is in card image folder, if not copy into it
            // Select image file to load
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = @m_ProjectRootPath,
                Title = "Select image file to load:",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "png",
                Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png",
                FilterIndex = 1,
                RestoreDirectory = true,

            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string uncheckedPath = openFileDialog.FileName;
                string uncheckedimage = Path.GetFileName(uncheckedPath);

                if (!File.Exists(m_ProjectRootPath + "\\CardImages\\" + uncheckedimage))
                {
                    File.Copy(uncheckedPath, m_ProjectRootPath + "\\CardImages\\" + uncheckedimage);
                }

                cardImage.ImageLocation = @uncheckedPath;

            }
            else
            {
                return;
            }
   
        }

        // Save as new button clicked
        private void saveAsNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JObject updatedObject = new JObject();

            if (CheckIfFillValid())
            {
                updatedObject.Add("name", nameValue.Text);
                updatedObject.Add("row", rowValue.Text);
                updatedObject.Add("originalPoint", pointValue.Value);
                updatedObject.Add("description", descValue.Text);
                updatedObject.Add("rarity", rarityValue.Text);

                updatedObject.Add("image", Path.GetFileName(cardImage.ImageLocation));

                // add ability list
                JArray abilityList = GetAbilityList();
                updatedObject.Add("ability", abilityList);

                m_CardList.Add(updatedObject);

                DataManager.SaveJsonFile(m_CurrentDeckPath, JArray.FromObject(m_CardList));

                ReloadCardList();
            }
            else
            {
                MessageBox.Show("Invalid Input in new card");
                return;
            }
            
        }

        private bool CheckIfFillValid()
        {
            bool isValid = !string.IsNullOrEmpty(nameValue.Text) && !string.IsNullOrEmpty(rowValue.Text) && 
                           !string.IsNullOrEmpty(descValue.Text) && !string.IsNullOrEmpty(rarityValue.Text) && 
                           !string.IsNullOrEmpty(cardImage.ImageLocation);

            return isValid;
        }

        private void ReloadCardList()
        {
            cardListBox.Items.Clear();
            if (m_CardList != null)
            {
                foreach (var card in m_CardList)
                {
                    cardListBox.Items.Add(card["name"]);
                }

            }
        }
        
        // New button clicked
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cardListBox.SelectedIndex = -1;
            // Clear the field for new card to edit
            nameValue.Clear();
            rowValue.SelectedIndex = -1;
            pointValue.Value = 0;
            rarityValue.SelectedIndex = -1;
            descValue.Clear();
            cardImage.ImageLocation = null;
            ResetAbilityList();
        }

        // Behavior control for Ability 2
        private void abilityValue2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Cannot set ability 2 while ability 1 is empty
            if (abilityValue1.SelectedIndex == -1)
            {
                MessageBox.Show("Set Ability 1 before Ability 2");
                abilityValue2.SelectedIndex = -1;
            }


        }
    }
}
