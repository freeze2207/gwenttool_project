﻿namespace GwentTools_Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.setProjectPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDeckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardListBox = new System.Windows.Forms.ListBox();
            this.nameValue = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.rowLabel = new System.Windows.Forms.Label();
            this.rowValue = new System.Windows.Forms.ComboBox();
            this.pointLabel = new System.Windows.Forms.Label();
            this.pointValue = new System.Windows.Forms.NumericUpDown();
            this.rarityLabel = new System.Windows.Forms.Label();
            this.rarityValue = new System.Windows.Forms.ComboBox();
            this.descroptionLabel = new System.Windows.Forms.Label();
            this.descValue = new System.Windows.Forms.RichTextBox();
            this.ability1label = new System.Windows.Forms.Label();
            this.abilityValue1 = new System.Windows.Forms.ComboBox();
            this.abilityValue2 = new System.Windows.Forms.ComboBox();
            this.ability2label = new System.Windows.Forms.Label();
            this.cardImage = new System.Windows.Forms.PictureBox();
            this.changeImageButton = new System.Windows.Forms.Button();
            this.imageSuggestText = new System.Windows.Forms.Label();
            this.notificationText = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pointValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardImage)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(822, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setProjectPathToolStripMenuItem,
            this.loadDeckToolStripMenuItem,
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsNewToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // setProjectPathToolStripMenuItem
            // 
            this.setProjectPathToolStripMenuItem.Name = "setProjectPathToolStripMenuItem";
            this.setProjectPathToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.setProjectPathToolStripMenuItem.Text = "Set Project Path";
            this.setProjectPathToolStripMenuItem.Click += new System.EventHandler(this.setProjectPathToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.saveToolStripMenuItem.Text = "Save to Current Card";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsNewToolStripMenuItem
            // 
            this.saveAsNewToolStripMenuItem.Name = "saveAsNewToolStripMenuItem";
            this.saveAsNewToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.saveAsNewToolStripMenuItem.Text = "Save as New Card";
            this.saveAsNewToolStripMenuItem.Click += new System.EventHandler(this.saveAsNewToolStripMenuItem_Click);
            // 
            // loadDeckToolStripMenuItem
            // 
            this.loadDeckToolStripMenuItem.Name = "loadDeckToolStripMenuItem";
            this.loadDeckToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.loadDeckToolStripMenuItem.Text = "Load Deck";
            this.loadDeckToolStripMenuItem.Click += new System.EventHandler(this.loadDeckToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // cardListBox
            // 
            this.cardListBox.FormattingEnabled = true;
            this.cardListBox.HorizontalScrollbar = true;
            this.cardListBox.Location = new System.Drawing.Point(0, 27);
            this.cardListBox.Name = "cardListBox";
            this.cardListBox.Size = new System.Drawing.Size(120, 69);
            this.cardListBox.TabIndex = 1;
            this.cardListBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            // 
            // nameValue
            // 
            this.nameValue.Location = new System.Drawing.Point(410, 76);
            this.nameValue.Name = "nameValue";
            this.nameValue.Size = new System.Drawing.Size(100, 20);
            this.nameValue.TabIndex = 3;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(344, 79);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(60, 13);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.Text = "Card Name";
            // 
            // rowLabel
            // 
            this.rowLabel.AutoSize = true;
            this.rowLabel.Location = new System.Drawing.Point(538, 82);
            this.rowLabel.Name = "rowLabel";
            this.rowLabel.Size = new System.Drawing.Size(54, 13);
            this.rowLabel.TabIndex = 6;
            this.rowLabel.Text = "Card Row";
            // 
            // rowValue
            // 
            this.rowValue.FormattingEnabled = true;
            this.rowValue.Items.AddRange(new object[] {
            "Melee",
            "Ranged",
            "Siege"});
            this.rowValue.Location = new System.Drawing.Point(598, 76);
            this.rowValue.Name = "rowValue";
            this.rowValue.Size = new System.Drawing.Size(121, 21);
            this.rowValue.TabIndex = 7;
            // 
            // pointLabel
            // 
            this.pointLabel.AutoSize = true;
            this.pointLabel.Location = new System.Drawing.Point(344, 146);
            this.pointLabel.Name = "pointLabel";
            this.pointLabel.Size = new System.Drawing.Size(56, 13);
            this.pointLabel.TabIndex = 8;
            this.pointLabel.Text = "Card Point";
            // 
            // pointValue
            // 
            this.pointValue.Location = new System.Drawing.Point(406, 144);
            this.pointValue.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.pointValue.Name = "pointValue";
            this.pointValue.Size = new System.Drawing.Size(104, 20);
            this.pointValue.TabIndex = 9;
            // 
            // rarityLabel
            // 
            this.rarityLabel.AutoSize = true;
            this.rarityLabel.Location = new System.Drawing.Point(538, 146);
            this.rarityLabel.Name = "rarityLabel";
            this.rarityLabel.Size = new System.Drawing.Size(59, 13);
            this.rarityLabel.TabIndex = 10;
            this.rarityLabel.Text = "Card Rarity";
            // 
            // rarityValue
            // 
            this.rarityValue.FormattingEnabled = true;
            this.rarityValue.Items.AddRange(new object[] {
            "normal",
            "rare",
            "epic"});
            this.rarityValue.Location = new System.Drawing.Point(598, 143);
            this.rarityValue.Name = "rarityValue";
            this.rarityValue.Size = new System.Drawing.Size(121, 21);
            this.rarityValue.TabIndex = 11;
            // 
            // descroptionLabel
            // 
            this.descroptionLabel.AutoSize = true;
            this.descroptionLabel.Location = new System.Drawing.Point(344, 205);
            this.descroptionLabel.Name = "descroptionLabel";
            this.descroptionLabel.Size = new System.Drawing.Size(85, 13);
            this.descroptionLabel.TabIndex = 12;
            this.descroptionLabel.Text = "Card Description";
            // 
            // descValue
            // 
            this.descValue.Location = new System.Drawing.Point(347, 229);
            this.descValue.Name = "descValue";
            this.descValue.Size = new System.Drawing.Size(372, 64);
            this.descValue.TabIndex = 13;
            this.descValue.Text = "";
            // 
            // ability1label
            // 
            this.ability1label.AutoSize = true;
            this.ability1label.Location = new System.Drawing.Point(344, 310);
            this.ability1label.Name = "ability1label";
            this.ability1label.Size = new System.Drawing.Size(43, 13);
            this.ability1label.TabIndex = 14;
            this.ability1label.Text = "Ability 1";
            // 
            // abilityValue1
            // 
            this.abilityValue1.FormattingEnabled = true;
            this.abilityValue1.Items.AddRange(new object[] {
            "",
            "Morale Boost",
            "Hero",
            "Double-Edge"});
            this.abilityValue1.Location = new System.Drawing.Point(347, 337);
            this.abilityValue1.Name = "abilityValue1";
            this.abilityValue1.Size = new System.Drawing.Size(121, 21);
            this.abilityValue1.TabIndex = 15;
            this.abilityValue1.SelectedIndexChanged += new System.EventHandler(this.abilityValue1_SelectedIndexChanged);
            // 
            // abilityValue2
            // 
            this.abilityValue2.FormattingEnabled = true;
            this.abilityValue2.Items.AddRange(new object[] {
            "",
            "Morale Boost",
            "Hero",
            "Double-Edge"});
            this.abilityValue2.Location = new System.Drawing.Point(541, 337);
            this.abilityValue2.Name = "abilityValue2";
            this.abilityValue2.Size = new System.Drawing.Size(121, 21);
            this.abilityValue2.TabIndex = 17;
            this.abilityValue2.SelectedIndexChanged += new System.EventHandler(this.abilityValue2_SelectedIndexChanged);
            // 
            // ability2label
            // 
            this.ability2label.AutoSize = true;
            this.ability2label.Location = new System.Drawing.Point(538, 310);
            this.ability2label.Name = "ability2label";
            this.ability2label.Size = new System.Drawing.Size(43, 13);
            this.ability2label.TabIndex = 16;
            this.ability2label.Text = "Ability 2";
            // 
            // cardImage
            // 
            this.cardImage.Location = new System.Drawing.Point(148, 76);
            this.cardImage.Name = "cardImage";
            this.cardImage.Size = new System.Drawing.Size(130, 130);
            this.cardImage.TabIndex = 18;
            this.cardImage.TabStop = false;
            // 
            // changeImageButton
            // 
            this.changeImageButton.Location = new System.Drawing.Point(173, 221);
            this.changeImageButton.Name = "changeImageButton";
            this.changeImageButton.Size = new System.Drawing.Size(75, 35);
            this.changeImageButton.TabIndex = 19;
            this.changeImageButton.Text = "Change Image";
            this.changeImageButton.UseVisualStyleBackColor = true;
            this.changeImageButton.Click += new System.EventHandler(this.changeImageButton_Click);
            // 
            // imageSuggestText
            // 
            this.imageSuggestText.Location = new System.Drawing.Point(145, 268);
            this.imageSuggestText.Name = "imageSuggestText";
            this.imageSuggestText.Size = new System.Drawing.Size(133, 40);
            this.imageSuggestText.TabIndex = 20;
            this.imageSuggestText.Text = "Please use 75x75 image\r\nfor best quality";
            this.imageSuggestText.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // notificationText
            // 
            this.notificationText.AutoSize = true;
            this.notificationText.Location = new System.Drawing.Point(403, 24);
            this.notificationText.Name = "notificationText";
            this.notificationText.Size = new System.Drawing.Size(131, 26);
            this.notificationText.TabIndex = 21;
            this.notificationText.Text = "Please set project root first\r\n(File -> Set Project Path)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(822, 488);
            this.Controls.Add(this.notificationText);
            this.Controls.Add(this.imageSuggestText);
            this.Controls.Add(this.changeImageButton);
            this.Controls.Add(this.cardImage);
            this.Controls.Add(this.abilityValue2);
            this.Controls.Add(this.ability2label);
            this.Controls.Add(this.abilityValue1);
            this.Controls.Add(this.ability1label);
            this.Controls.Add(this.descValue);
            this.Controls.Add(this.descroptionLabel);
            this.Controls.Add(this.rarityValue);
            this.Controls.Add(this.rarityLabel);
            this.Controls.Add(this.pointValue);
            this.Controls.Add(this.pointLabel);
            this.Controls.Add(this.rowValue);
            this.Controls.Add(this.rowLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.nameValue);
            this.Controls.Add(this.cardListBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Gwent Card Edior";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pointValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDeckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ListBox cardListBox;
        private System.Windows.Forms.TextBox nameValue;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label rowLabel;
        private System.Windows.Forms.ComboBox rowValue;
        private System.Windows.Forms.Label pointLabel;
        private System.Windows.Forms.NumericUpDown pointValue;
        private System.Windows.Forms.Label rarityLabel;
        private System.Windows.Forms.ComboBox rarityValue;
        private System.Windows.Forms.Label descroptionLabel;
        private System.Windows.Forms.RichTextBox descValue;
        private System.Windows.Forms.Label ability1label;
        private System.Windows.Forms.ComboBox abilityValue1;
        private System.Windows.Forms.ComboBox abilityValue2;
        private System.Windows.Forms.Label ability2label;
        private System.Windows.Forms.PictureBox cardImage;
        private System.Windows.Forms.Button changeImageButton;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setProjectPathToolStripMenuItem;
        private System.Windows.Forms.Label imageSuggestText;
        private System.Windows.Forms.Label notificationText;
    }
}

