﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GwentTools_Project
{
    class DataManager
    {
        public static bool CheckJsonFile(string _filePath)
        {
            // Check Json file if valid
            try
            {
                string content = System.IO.File.ReadAllText(@_filePath);
                JArray array = JArray.Parse(content);
                foreach (var card in array)
                {
                    JObject tempCard = JObject.FromObject(card);

                    if (!tempCard.ContainsKey("row") || !tempCard.ContainsKey("originalPoint"))
                    {
                        throw new Exception("Wrong Json");
                    }

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Json or Empty Json");
                return false;
                throw;
            }

            return true;
        }

        public static List<JObject> ReadJsonFile(string _filePath)
        {
            if(!CheckJsonFile(_filePath))
            {
                return null;
            }
            else
            {
                string content = System.IO.File.ReadAllText(@_filePath);

                List<JObject> completeCardArray = new List<JObject>();

                foreach (var card in JArray.Parse(content))
                {
                    completeCardArray.Add(JObject.FromObject(card));
                }

                return completeCardArray;
            }
            
        }

        public static void SaveJsonFile(string _filePath, JArray _jsonArray)
        {
            System.IO.File.WriteAllText(@_filePath, _jsonArray.ToString());
        }

    }
}
